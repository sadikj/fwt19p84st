function uvecaj(brojKartice){
  if(brojKartice==='prvi' || brojKartice==='drugi' || brojKartice==='treci' || brojKartice==='cetvrti' 
  || brojKartice==='peti' || brojKartice==='sesti' || brojKartice==='sedmi' || brojKartice==='osmi'  //if kojim predstavlja jedan od nacina da se zadatak uradi samo sa jednom funkcijom
  || brojKartice==='deveti' || brojKartice==='deseti'){
    if(brojKartice==='prvi'){          //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime')[0].style.display = 'none'; //prvo što radimo jeste da sve elemente grida osim slike sakrivamo "hide"
      document.getElementsByClassName('titula')[0].style.display = 'none';      //a to su ime_prezime, titula, broj_kancelarije, linkovi
      document.getElementsByClassName('broj_kancelarije')[0].style.display = 'none';
      document.getElementsByClassName('linkovi')[0].style.display = 'none';
      document.getElementsByClassName('slika')[0].style.gridRow = '1/6';       //sada mijenjamo poziciju grida slike i stavljamo je preko cijelog elementa tacnije od 1 do 6 
      document.getElementById('slika1').style.width = '100%';                  //nakon toga pristupamo i samoj slici prego njenog id-a te njoj povecavamo width i height
      document.getElementById('slika1').style.height = '100%';                 //što će ujedno sliku postaviti preko cijelog grida
      document.getElementById('slika1').style.margin = '0px';                  //da bi se slika raširila skroz potrebno je poništiti margine koje smo prethodno u css-u stavili
      document.getElementById('slika1').style.border = '0px';                  //također vrijedi i za border
      document.getElementById('slika1').style.borderRadius = '10px';           //stavljamo border-radius da bi se slika u potpunosti poklapala sa gridom na kojem smo već prethodno stavljali boreder-radius              
    }   
    else if(brojKartice==='drugi'){   //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime')[1].style.display = 'none';       //prethodni postupak se ponavlja za svakog profesora i asistenta pa prema tome nema ni smisla komentarisati jedno te isto 
      document.getElementsByClassName('titula')[1].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije')[1].style.display = 'none';
      document.getElementsByClassName('linkovi')[1].style.display = 'none';
      document.getElementsByClassName('slika')[1].style.gridRow = '1/6';
      document.getElementById('slika2').style.width = '100%';
      document.getElementById('slika2').style.height = '100%';
      document.getElementById('slika2').style.margin = '0px';
      document.getElementById('slika2').style.border = '0px';
      document.getElementById('slika2').style.borderRadius = '10px';
    }
    else if(brojKartice==='treci'){   //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime')[2].style.display = 'none';
      document.getElementsByClassName('titula')[2].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije')[2].style.display = 'none';
      document.getElementsByClassName('linkovi')[2].style.display = 'none';
      document.getElementsByClassName('slika')[2].style.gridRow = '1/6';
      document.getElementById('slika3').style.width = '100%';
      document.getElementById('slika3').style.height = '100%';
      document.getElementById('slika3').style.margin = '0px';
      document.getElementById('slika3').style.border = '0px';
      document.getElementById('slika3').style.borderRadius = '10px';
    }
    else if(brojKartice==='cetvrti'){   //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime')[3].style.display = 'none';
      document.getElementsByClassName('titula')[3].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije')[3].style.display = 'none';
      document.getElementsByClassName('linkovi')[3].style.display = 'none';
      document.getElementsByClassName('slika')[3].style.gridRow = '1/6';
      document.getElementById('slika4').style.width = '100%';
      document.getElementById('slika4').style.height = '100%';
      document.getElementById('slika4').style.margin = '0px';
      document.getElementById('slika4').style.border = '0px';
      document.getElementById('slika4').style.borderRadius = '10px';
    }
    else if(brojKartice==='peti'){      //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime1')[0].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije1')[0].style.display = 'none';
      document.getElementsByClassName('linkovi1')[0].style.display = 'none';
      document.getElementsByClassName('slika1')[0].style.gridRow = '1/5';
      document.getElementById('slika5').style.width = '100%';
      document.getElementById('slika5').style.height = '100%';
      document.getElementById('slika5').style.margin = '0px';
      document.getElementById('slika5').style.border = '0px';
      document.getElementById('slika5').style.borderRadius = '10px';
    }
    else if(brojKartice==='sesti'){     //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje 
      document.getElementsByClassName('ime_prezime1')[1].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije1')[1].style.display = 'none';
      document.getElementsByClassName('linkovi1')[1].style.display = 'none';
      document.getElementsByClassName('slika1')[1].style.gridRow = '1/5';
      document.getElementById('slika6').style.width = '100%';
      document.getElementById('slika6').style.height = '100%';
      document.getElementById('slika6').style.margin = '0px';
      document.getElementById('slika6').style.border = '0px';
      document.getElementById('slika6').style.borderRadius = '10px';
    }
    else if(brojKartice==='sedmi'){   //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime1')[2].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije1')[2].style.display = 'none';
      document.getElementsByClassName('linkovi1')[2].style.display = 'none';
      document.getElementsByClassName('slika1')[2].style.gridRow = '1/5';
      document.getElementById('slika7').style.width = '100%';
      document.getElementById('slika7').style.height = '100%';
      document.getElementById('slika7').style.margin = '0px';
      document.getElementById('slika7').style.border = '0px';
      document.getElementById('slika7').style.borderRadius = '10px';
    }
    else if(brojKartice==='osmi'){      //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime1')[3].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije1')[3].style.display = 'none';
      document.getElementsByClassName('linkovi1')[3].style.display = 'none';
      document.getElementsByClassName('slika1')[3].style.gridRow = '1/5';
      document.getElementById('slika8').style.width = '100%';
      document.getElementById('slika8').style.height = '100%';
      document.getElementById('slika8').style.margin = '0px';
      document.getElementById('slika8').style.border = '0px';
      document.getElementById('slika8').style.borderRadius = '10px';
    }
    else if(brojKartice==='deveti'){      //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime1')[4].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije1')[4].style.display = 'none';
      document.getElementsByClassName('linkovi1')[4].style.display = 'none';
      document.getElementsByClassName('slika1')[4].style.gridRow = '1/5';
      document.getElementById('slika9').style.width = '100%';
      document.getElementById('slika9').style.height = '100%';
      document.getElementById('slika9').style.margin = '0px';
      document.getElementById('slika9').style.border = '0px';
      document.getElementById('slika9').style.borderRadius = '10px';
    }
    else if(brojKartice==='deseti'){      //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo povecanje
      document.getElementsByClassName('ime_prezime1')[5].style.display = 'none';
      document.getElementsByClassName('broj_kancelarije1')[5].style.display = 'none';
      document.getElementsByClassName('linkovi1')[5].style.display = 'none';
      document.getElementsByClassName('slika1')[5].style.gridRow = '1/5';
      document.getElementById('slika10').style.width = '100%';
      document.getElementById('slika10').style.height = '100%';
      document.getElementById('slika10').style.margin = '0px';
      document.getElementById('slika10').style.border = '0px';
      document.getElementById('slika10').style.borderRadius = '10px';
    } 
  }
  else{
    if(brojKartice==='prvi1'){      //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime')[0].style.display = 'grid';       //sada za razliku od prethondog slučaja kada smo hidali elemente sada ih trebamo učiti vidljivima tako što ih prikazuemo kao grid
      document.getElementsByClassName('titula')[0].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije')[0].style.display = 'grid';
      document.getElementsByClassName('linkovi')[0].style.display = 'grid';
      document.getElementsByClassName('slika')[0].style.gridRow = '1/2';              //vraćamo poziciju grida slike i stavljamo je da bude od 1 do 2 kao što je i ranije bila     
      document.getElementById('slika1').style.width = '108px';                        //vraćamo i width i height od orginalne slike
      document.getElementById('slika1').style.height = '192px';
      document.getElementById('slika1').style.marginTop = '5px';                      //postavljamo margine radi ekstetike
      document.getElementById('slika1').style.marginBottom = '5px';
      document.getElementById('slika1').style.marginLeft = 'auto';                    //ovdje smo centrirali sliku
      document.getElementById('slika1').style.marginRight = 'auto';
      document.getElementById('slika1').style.border = '3px solid black';             //vraćamo border postavljen u css-u od ranije
      document.getElementById('slika1').style.borderRadius = '0px';                   //te border-radius stavljamo na 0px kao što je ranije bilo u css-u
    }       
    else if(brojKartice==='drugi1'){    //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime')[1].style.display = 'grid';
      document.getElementsByClassName('titula')[1].style.display = 'grid';            //postupak se ponavlja na svakom od if-ova
      document.getElementsByClassName('broj_kancelarije')[1].style.display = 'grid';
      document.getElementsByClassName('linkovi')[1].style.display = 'grid';
      document.getElementsByClassName('slika')[1].style.gridRow = '1/2';
      document.getElementById('slika2').style.width = '108px';
      document.getElementById('slika2').style.height = '192px';
      document.getElementById('slika2').style.marginTop = '5px';
      document.getElementById('slika2').style.marginBottom = '5px';
      document.getElementById('slika2').style.marginLeft = 'auto';
      document.getElementById('slika2').style.marginRight = 'auto';
      document.getElementById('slika2').style.border = '3px solid black';
      document.getElementById('slika2').style.borderRadius = '0px';
    }
    else if(brojKartice==='treci1'){      //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime')[2].style.display = 'grid';
      document.getElementsByClassName('titula')[2].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije')[2].style.display = 'grid';
      document.getElementsByClassName('linkovi')[2].style.display = 'grid';
      document.getElementsByClassName('slika')[2].style.gridRow = '1/2';
      document.getElementById('slika3').style.width = '108px';
      document.getElementById('slika3').style.height = '192px';
      document.getElementById('slika3').style.marginTop = '5px';
      document.getElementById('slika3').style.marginBottom = '5px';
      document.getElementById('slika3').style.marginLeft = 'auto';
      document.getElementById('slika3').style.marginRight = 'auto';
      document.getElementById('slika3').style.border = '3px solid black';
      document.getElementById('slika3').style.borderRadius = '0px';
    }
    else if(brojKartice==='cetvrti1'){      //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime')[3].style.display = 'grid';
      document.getElementsByClassName('titula')[3].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije')[3].style.display = 'grid';
      document.getElementsByClassName('linkovi')[3].style.display = 'grid';
      document.getElementsByClassName('slika')[3].style.gridRow = '1/2';
      document.getElementById('slika4').style.width = '108px';
      document.getElementById('slika4').style.height = '192px';
      document.getElementById('slika4').style.marginTop = '5px';
      document.getElementById('slika4').style.marginBottom = '5px';
      document.getElementById('slika4').style.marginLeft = 'auto';
      document.getElementById('slika4').style.marginRight = 'auto';
      document.getElementById('slika4').style.border = '3px solid black';
      document.getElementById('slika4').style.borderRadius = '0px';
    }
    else if(brojKartice==='peti1'){       //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime1')[0].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije1')[0].style.display = 'grid';
      document.getElementsByClassName('linkovi1')[0].style.display = 'grid';
      document.getElementsByClassName('slika1')[0].style.gridRow = '1/2';
      document.getElementById('slika5').style.width = '108px';
      document.getElementById('slika5').style.height = '192px';
      document.getElementById('slika5').style.marginTop = '5px';
      document.getElementById('slika5').style.marginBottom = '5px';
      document.getElementById('slika5').style.marginLeft = 'auto';
      document.getElementById('slika5').style.marginRight = 'auto';
      document.getElementById('slika5').style.border = '3px solid black';
      document.getElementById('slika5').style.borderRadius = '0px';
    }
    else if(brojKartice==='sesti1'){      //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime1')[1].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije1')[1].style.display = 'grid';
      document.getElementsByClassName('linkovi1')[1].style.display = 'grid';
      document.getElementsByClassName('slika1')[1].style.gridRow = '1/2';
      document.getElementById('slika6').style.width = '108px';
      document.getElementById('slika6').style.height = '192px';
      document.getElementById('slika6').style.marginTop = '5px';
      document.getElementById('slika6').style.marginBottom = '5px';
      document.getElementById('slika6').style.marginLeft = 'auto';
      document.getElementById('slika6').style.marginRight = 'auto';
      document.getElementById('slika6').style.border = '3px solid black';
      document.getElementById('slika6').style.borderRadius = '0px';
    }
    else if(brojKartice==='sedmi1'){
      document.getElementsByClassName('ime_prezime1')[2].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije1')[2].style.display = 'grid';
      document.getElementsByClassName('linkovi1')[2].style.display = 'grid';
      document.getElementsByClassName('slika1')[2].style.gridRow = '1/2';
      document.getElementById('slika7').style.width = '108px';
      document.getElementById('slika7').style.height = '192px';
      document.getElementById('slika7').style.marginTop = '5px';
      document.getElementById('slika7').style.marginBottom = '5px';
      document.getElementById('slika7').style.marginLeft = 'auto';
      document.getElementById('slika7').style.marginRight = 'auto';
      document.getElementById('slika7').style.border = '3px solid black';
      document.getElementById('slika7').style.borderRadius = '0px';
    }
    else if(brojKartice==='osmi1'){       //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime1')[3].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije1')[3].style.display = 'grid';
      document.getElementsByClassName('linkovi1')[3].style.display = 'grid';
      document.getElementsByClassName('slika1')[3].style.gridRow = '1/2';
      document.getElementById('slika8').style.width = '108px';
      document.getElementById('slika8').style.height = '192px';
      document.getElementById('slika8').style.marginTop = '5px';
      document.getElementById('slika8').style.marginBottom = '5px';
      document.getElementById('slika8').style.marginLeft = 'auto';
      document.getElementById('slika8').style.marginRight = 'auto';
      document.getElementById('slika8').style.border = '3px solid black';
      document.getElementById('slika8').style.borderRadius = '0px';
    }
    else if(brojKartice==='deveti1'){     //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime1')[4].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije1')[4].style.display = 'grid';
      document.getElementsByClassName('linkovi1')[4].style.display = 'grid';
      document.getElementsByClassName('slika1')[4].style.gridRow = '1/2';
      document.getElementById('slika9').style.width = '108px';
      document.getElementById('slika9').style.height = '192px';
      document.getElementById('slika9').style.marginTop = '5px';
      document.getElementById('slika9').style.marginBottom = '5px';
      document.getElementById('slika9').style.marginLeft = 'auto';
      document.getElementById('slika9').style.marginRight = 'auto';
      document.getElementById('slika9').style.border = '3px solid black';
      document.getElementById('slika9').style.borderRadius = '0px';
    }
    else if(brojKartice==='deseti1'){     //ovim if-om provjeravamo o kojoj slici je rijec i na njoj cemo da vrsimo smanjenje
      document.getElementsByClassName('ime_prezime1')[5].style.display = 'grid';
      document.getElementsByClassName('broj_kancelarije1')[5].style.display = 'grid';
      document.getElementsByClassName('linkovi1')[5].style.display = 'grid';
      document.getElementsByClassName('slika1')[5].style.gridRow = '1/2';
      document.getElementById('slika10').style.width = '108px';
      document.getElementById('slika10').style.height = '192px';
      document.getElementById('slika10').style.marginTop = '5px';
      document.getElementById('slika10').style.marginBottom = '5px';
      document.getElementById('slika10').style.marginLeft = 'auto';
      document.getElementById('slika10').style.marginRight = 'auto';
      document.getElementById('slika10').style.border = '3px solid black';
      document.getElementById('slika10').style.borderRadius = '0px';
    }
  } 
}








function sortiraj() {
  /*Zadatak sam uradio na nacin da sam sortirao li-ove koji su dati u html-u, nakon toga mozemo zakljuciti da se li-ovi jesu 
  sortirali ali elementi u gridu nisu pa sam prema tome svaki put kad nadjem da se li sortira, sortirao i element grida tj. mijenjo njihovu 
  poziciju.*/
  document.getElementById('FWT').style.marginTop = '0px';        //radi estetike posto bi nakon zamijene na ovim mjestima bio veci razmak nego na ostalma
  document.getElementById('ASP').style.marginBottom = '0px';     //zbog margin top i bottom
  let varijabla = document.getElementById("pocetakJeTezak");
  let provjera = true;
  let zamjena = false;
  let j=0;
  while (provjera) {                                    //while petlja koja ce da se vrti sve dok postoji element koji se moze zamijeniti
    provjera = false;
    let b = varijabla.getElementsByClassName("premeci");  //klasa koja se nalazi na svakom od predmeta
    for (let i = 0; i < (b.length - 1); i++) {            //provjeravamo da li postoji element potecijalan za sortiranje
      zamjena = false;
      if ((b[i].id.toString()).localeCompare(b[i + 1].id.toString())===1) {   //provjera se vrsi pomocu id-ova
        zamjena = true;                                                       //id-ove pretvaramo u stringove i pomocu funkcije localCompare koja 
        j=i;                                                                  //poredi stringove te ukoliko je uslov zadovoljen vrsimo zamijenu 
        break;
      }
    }
    if (zamjena) {
      b[j].parentNode.insertBefore(b[j + 1], b[j]);         //na ovom mjestu vrsimo ubacivanje elementa b[j] koji je po leksikografskom redu prije
      provjera = true;                                      //provjeru vracamo na true kako bi vanjska while petlja ponovno trazila novi potencijali element za zamijenu
          b[j+1].style.gridRow = (j+2)+"/"+(j+3);           //na ovom mjestu vrsimo sortiranje gridova tako sto zamijenimo njihove pozcije 
          b[j].style.gridRow = (j+1)+"/"+(j+2);             //tj. onaj sto je bio ispred bit ce iza, a onaj sto je bio iza bit ce ispred
    }
  }
  document.getElementById('ASP').style.marginTop = '7px';   //za elemente koji ce biti na vrhu i na dnu stavljamo margin botom i top radi izgleda
  document.getElementById('WT').style.marginBottom = '7px';
  let maza = document.getElementsByClassName('okvir1');
  maza[0].style.gridGap = "0px";  
  let margina = document.getElementsByClassName('premeci'); 
  for(let i=0; i<margina.length; i++){                   //nacin na koji vracamo rastojanje je tako sto ubacimo marginTop na svaki element klase
    margina[i].style.marginTop = "15px";                 //koja je zajednicka klasa za sve predmete
  }
}






function sakriPokazi(element){                                 //if kojim predstavlja jedan od nacina da se zadatak uradi samo sa jednom funkcijom
    if(element==='prva'){                                      //posaljemo proizvoljni string i u zavisnosti koji smo poslali mozemo zakljuciti o kom slucaju je rijec
        document.getElementById('WT').style.display='none';    //svi elementi su vec prikazani kao display:grid
        document.getElementById('RMA').style.display='none';   //sada to mijenjamo tako sto pristupamo svakom predmetu preko id-ova i postavimo ga da nam sakrije sadrzaj 
        document.getElementById('ASP').style.display='none';   //preko komande style.display = 'none';
        let maza = document.getElementsByClassName('okvir1');
        maza[0].style.gridGap = "0px";  
        let margina = document.getElementsByClassName('premeci');   //tj. ukoliko izbacimo 2 elementa grida koji su jedan ispod drugog rastojanje nece biti 15px vec 30px
        for(let i=0; i<margina.length; i++){                   //nacin na koji vracamo rastojanje je tako sto ubacimo marginTop na svaki element klase
          margina[i].style.marginTop = "15px";                 //koja je zajednicka klasa za sve predmete
        }
        document.getElementById('FWT').style.display='grid';    //ostali elementi se prikazuju
        document.getElementById('TP').style.display='grid';
        document.getElementById('UUP').style.display='grid';
        document.getElementById('UBP').style.display='grid';
        document.getElementById('VIS').style.display='grid';
        document.getElementById('MUR1').style.display='grid';
        document.getElementById('RS').style.display='grid';
        document.getElementById('RAM').style.display='grid';
    }
    else if(element==='druga'){
        document.getElementById('FWT').style.display='none';    //identicno kao i za prvu godinu tako radimo i sa drugom
        document.getElementById('TP').style.display='none';
        document.getElementById('UUP').style.display='none';
        document.getElementById('UBP').style.display='none';
        document.getElementById('VIS').style.display='none';
        document.getElementById('MUR1').style.display='none';
        document.getElementById('RS').style.display='none';
        document.getElementById('RAM').style.display='none';
        let maza = document.getElementsByClassName('okvir1');
        maza[0].style.gridGap = "0px";
        let margina = document.getElementsByClassName('premeci');
        for(let i=0; i<margina.length; i++){
          margina[i].style.marginTop = "15px";
        }
        document.getElementById('WT').style.display='grid';    
        document.getElementById('RMA').style.display='grid';   
        document.getElementById('ASP').style.display='grid'; 
    }
    else if (element==='svi'){
        document.getElementById('WT').style.display='grid';   //isto kao i prethodna dva slucaja samo sad trebamo da pokazemo elemente
        document.getElementById('RMA').style.display='grid';  //to radimo preko style.display='grid' posto je rijec o gridu
        document.getElementById('ASP').style.display='grid';
        document.getElementById('FWT').style.display='grid';
        document.getElementById('TP').style.display='grid';
        document.getElementById('UUP').style.display='grid';
        document.getElementById('UBP').style.display='grid';
        document.getElementById('VIS').style.display='grid';
        document.getElementById('MUR1').style.display='grid';
        document.getElementById('RS').style.display='grid';
        document.getElementById('RAM').style.display='grid';
        let maza = document.getElementsByClassName('okvir1');
        maza[0].style.gridGap = "0px";
        let margina = document.getElementsByClassName('premeci');
        for(let i=0; i<margina.length; i++){
          margina[i].style.marginTop = "15px";
        }
    }
}








function oboji(element){
  /*Postavio sam klase za svakog od profesora u zavisnosti da li misem prelazimo preko celije ili ne. Nakon toga pristupamo toj klasi i za
  sve elemente te klase postavljamo zeljenu boju (zutu). Tako za svakog od profesora. */
  if(element==='profVO'){                                       
    let varijabla = document.getElementsByClassName('prof1');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profVO1'){
    let varijabla = document.getElementsByClassName('prof1');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profZJ'){
    let varijabla = document.getElementsByClassName('prof2');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profZJ1'){
    let varijabla = document.getElementsByClassName('prof2');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profEB'){
    let varijabla = document.getElementsByClassName('prof3');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profEB1'){
    let varijabla = document.getElementsByClassName('prof3');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profAA'){
    let varijabla = document.getElementsByClassName('prof4');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profAA1'){
    let varijabla = document.getElementsByClassName('prof4');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profVL'){
    let varijabla = document.getElementsByClassName('prof5');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profVL1'){
    let varijabla = document.getElementsByClassName('prof5');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profAMR'){
    let varijabla = document.getElementsByClassName('prof6');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profAMR1'){
    let varijabla = document.getElementsByClassName('prof6');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profFD'){
    let varijabla = document.getElementsByClassName('prof7');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profFD1'){
    let varijabla = document.getElementsByClassName('prof7');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profSR'){
    let varijabla = document.getElementsByClassName('prof8');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profSR1'){
    let varijabla = document.getElementsByClassName('prof8');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profDB'){
    let varijabla = document.getElementsByClassName('prof9');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profDB1'){
    let varijabla = document.getElementsByClassName('prof9');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profIB'){
    let varijabla = document.getElementsByClassName('prof10');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profIB1'){
    let varijabla = document.getElementsByClassName('prof10');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profDD'){
    let varijabla = document.getElementsByClassName('prof11');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profDD1'){
    let varijabla = document.getElementsByClassName('prof11');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
  else if(element==='profAT'){
    let varijabla = document.getElementsByClassName('prof12');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "yellow";
    }
  }
  else if(element==='profAT1'){
    let varijabla = document.getElementsByClassName('prof12');
    for(let i=0; i<varijabla.length; i++){
      varijabla[i].style.background = "lemonchiffon";
    }
  }
}