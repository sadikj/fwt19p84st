var Pretraga = (function(){
    let predmeti;                                           //privatni atribut modula Pretraga
    let postaviElemente = function(parametar){              //postavi elemente je metoda koja prima HTML elemente koje joj šaljemo iz file "godine.js" 
        predmeti = parametar;
        return predmeti;                                    //povratna vrijednost metode su HTML elementi koje ćemo kasnije koristiti za pristupanje različitim dijelovima HTML dokumenta te uzimati vrijednosti koje su nam neophodne  
    }
    let pretragaPredmet = function(nazivPredmeta){          //metoda koja prima parametar "nazivPredmeta" koji nam služi kao vrijednost za pretragu
        nazivPredmeta = nazivPredmeta.toString().toLowerCase();     //radi sigurnosti pretvaramo ga u string i u mala slova jer pretraga nije osjetljiva na velika i mala slova
        for(let i=0; i<predmeti.length; i++){               //for petlja koja služi da prikaže sve "predmete" ukoliko se je desilo da je korištenjem druge pretrage neki od "predmeta" bio "none"
            predmeti[i].style.display = "";
        }
        if(nazivPredmeta===''){                             //ukoliko se pošalje prazan string svi "predmeti" se pokazuju
            for(let i=0; i<predmeti.length; i++){
                predmeti[i].style.display = "";
            }     
        }
        else{
            for(let i=0; i<predmeti.length; i++){           //sada pristupamo vrijednostima "naziva predmeta" i "opisa predmeta" te provejravamo da li je "nazivPredmeta" substring od vrijednosti "naziv" i "opis" predmeta
                if((((predmeti[i].getElementsByClassName('naziv_predmeta')[0].textContent.toLowerCase()).includes(nazivPredmeta))===false) &&
                (((predmeti[i].getElementsByClassName('opis_predmeta')[0].textContent.toLowerCase()).includes(nazivPredmeta))===false)){       //funkcija provjere substringa "include()"
                    predmeti[i].style.display = "none";     //ukoliko jeste substring onda taj "predmet" sakrijemo "none"
                }
            }                            
        }  
    }
    let pretragaNastavnik = function(imeNastavnika){        //za pretragu po "nastavniku" važi sve kao i za "predmete" 
        imeNastavnika = imeNastavnika.toString().toLowerCase();
        for(let i=0; i<predmeti.length; i++){
            predmeti[i].style.display = "";
        }
        if(imeNastavnika===''){
            for(let i=0; i<predmeti.length; i++){
                predmeti[i].style.display = "";
            }     
        }
        else{
            for(let i=0; i<predmeti.length; i++){
                if(((predmeti[i].getElementsByClassName('ime_nastavnika')[0].textContent.toLowerCase()).includes(imeNastavnika))===false){
                    predmeti[i].style.display = "none";
                } 
            }                            
        }
    }
    let pretragaGodina = function (godina){
        for(let i=0; i<predmeti.length; i++){           //prikazali smo sve kao i u prethodna dva slučaja ako je korištenjem neke druge metode neki od "predmeta bio "none
            predmeti[i].style.display = "";
        }
        if(godina===1){                                 //ukoliko je riječ "godina===1" sve predmete koji imaju nemaju klasu "predmeti_prva" smo sakrili "none" 
            for(let i=0; i<predmeti.length; i++){
                if(!predmeti[i].getElementsByClassName('predmeti_prva')[0])
                    predmeti[i].style.display = "none";
            }
        }
        else if(godina===2){                            //ukoliko je riječ "godina===2" sve predmete koji imaju nemaju klasu "predmeti_druga" smo sakrili "none"
            for(let i=0; i<predmeti.length; i++){
                if(!predmeti[i].getElementsByClassName('predmeti_druga')[0])
                    predmeti[i].style.display = "none";
            }
        }
        else{
            for(let i=0; i<predmeti.length; i++){       //za sve ostale unesene vrijednosti smo prikazaliu sve predmete
                predmeti[i].style.display = "";
            }
        }
    }
    return{
        postaviElemente: postaviElemente,               //kao return vraćamo sve metodode iz modula
        pretragaPredmet: pretragaPredmet,
        pretragaNastavnik: pretragaNastavnik,
        pretragaGodina: pretragaGodina
    }
    
}());


