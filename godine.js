Pretraga.postaviElemente(document.getElementsByClassName('predmeti'));          //u "pretraga.js" šaljemo sve HTML elemente koji su članice klase "predmeti" 


function pretragaPoPredmetu(){
    Pretraga.pretragaPredmet(document.getElementById('po_predmetu').value);     //funkcija koju ćemo da pozivamo u HTML dokumentu u kojoj smo pozvali modul Pretraga i metodu "pretragaPredmet" te proslijedili vrijednost unesenu u input polje "onkeyup"
}

function pretragaPoProfesoru(){
    Pretraga.pretragaNastavnik(document.getElementById('po_nastavniku').value); //funkcija koju ćemo da pozivamo u HTML dokumentu u kojoj smo pozvali modul Pretraga i metodu "pretragaNastavnik" te proslijedili vrijednost unesenu u input polje "onkeyup"
}

function pretragaPoGodini(){
    Pretraga.pretragaGodina(parseFloat(document.getElementById('po_godini').value));  //funkcija koju ćemo da pozivamo u HTML dokumentu u kojoj smo pozvali modul Pretraga i metodu "pretragaGodina" te proslijedili vrijednost unesenu u input polje "onkeyup"
}



