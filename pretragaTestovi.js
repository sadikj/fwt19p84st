//testovi definisani za metodu Pretraga

let assert = chai.assert;
describe('Pretraga', function() {
    describe('pretragaPredmet()', function() {
        it('Treba da prikaže sve predmete kada je unesen prazan string u metodu pretragaPredmet', function() {
            let predmeti = document.getElementsByClassName("predmeti");         //pretraga koja ispituje prazan string 
            Pretraga.postaviElemente(predmeti);                                 //pošaljemo sve HTML elemente tj. "predmete" u metodu "postaviElemente"
            let brojac = 0;                                                     //definišemo "brojac" koji stavljamo da bude 0
            Pretraga.pretragaPredmet("");                                       //sada u metodu "pretragaPredmet" šaljemo prazan string
            for(let i=0; i<predmeti.length; i++){                               //for petljom provjeravamo da li su svi predmeti pokazani tj. display = ""
                if(predmeti[i].style.display === ""){                           //ukoliko je ispunjen uslov brojac povećavamo za 1
                    brojac = brojac + 1;
                }
            }                                                                   //na kraju ukoliko je brojac jendak duzini predmeta.length znaci da su svi predmeti prikazani i da je uslov ispunjen 
            assert.equal(brojac, predmeti.length,"Broj predmeta treba biti 19");
        });
        it('Treba da prikaže samo jedan predmet kada je unesen tačan naziv predmeta u metodu pretragaPredmet', function() {
            let predmeti = document.getElementsByClassName("predmeti");         //početak svakog testa je identičan pa da ne ponavljamo stavri samo ću objašnjavati postupak provjere
            Pretraga.postaviElemente(predmeti);
            let brojac = 0;
            Pretraga.pretragaPredmet("Frontend web tehnologije");               //šaljemo tačan naziv predmeta
            for(let i=0; i<predmeti.length; i++){                               //brojac treba samo da nađe jedan takav predmet
                if(predmeti[i].style.display === ""){                           
                    brojac = brojac + 1;
                }
            }                                                                   //ukoliko je nađen samo jedan takav predmet uslov je ispunjen
            assert.equal(brojac, 1,"Broj predmeta treba biti 1");
        });
        it('Treba da su skriveni svi predmeti kada unesemo riječ koje se ne sadrži u nazivu i opisu predmeta za metodu pretragaPredmet', function() {
            let predmeti = document.getElementsByClassName("predmeti");
            Pretraga.postaviElemente(predmeti);
            let brojac = 0;
            Pretraga.pretragaPredmet("Metodologija");                           //šaljemo predmet kojeg nema u ponuđenim predmetima, brojaca treba biti 0
            for(let i=0; i<predmeti.length; i++){                               //to znači da svi predmeti moraju biti skriveni što je tačno u mom slučaju
                if(!predmeti[i].style.display === ""){
                    brojac = brojac + 1;
                }
            }
            assert.equal(brojac, 0,"Broj predmeta treba biti 0");
        });
    });
    describe('pretragaNastavnik()', function() {
        it('Provjera da nije prikazan niti jedan predmet kada se proslijedi imeNastavnika koje ne postoji niti za jedan predmet', function() {
            let predmeti = document.getElementsByClassName("predmeti");
            Pretraga.postaviElemente(predmeti);
            let brojac = 0;                                                     //šaljemo ime nastavnika koje ne postoji brojač treba da bude 0
            Pretraga.pretragaNastavnik("Tomica");                               //u našem slučajuje to ispunjeno 
            for(let i=0; i<predmeti.length; i++){
                if(!predmeti[i].style.display === ""){
                    brojac = brojac + 1;
                }
            }
            assert.equal(brojac, 0,"Broj predmeta treba biti 0");
          });
          it('Provjera da se prikazuju samo predmeti nekog profesora kada se proslijedi cijelo njegovo ime i prezime', function() {
            let predmeti = document.getElementsByClassName("predmeti");
            Pretraga.postaviElemente(predmeti);
            let brojac = 0;                                                     //provjeravamo da za tačan naziv profesora treba prikazati samo njegove predmete
            Pretraga.pretragaNastavnik("Vensada Okanović");
            for(let i=0; i<predmeti.length; i++){                               //pošto profesorica Vensada predaje 3 predmeta brojač treba biti 3, tačno u našem slučaju
                if(predmeti[i].style.display === ""){
                    brojac = brojac + 1;
                }
            }
            assert.equal(brojac, 3,"Broj predmeta treba biti 3");
          });
          it('Provjera da se prikazuju samo predmeti nekog profesora kada se proslijedi cijelo njegovo ime i prezime, ako su unesena sva mala slova', function() {
            let predmeti = document.getElementsByClassName("predmeti");
            Pretraga.postaviElemente(predmeti);
            let brojac = 0;                                                     //provjera ukoliko unesemo sve mala slova da treba metoda da radi 
            Pretraga.pretragaNastavnik("željko jurić");
            for(let i=0; i<predmeti.length; i++){                               //prof. Jurić nam predaje 3 predmeta i provjeravamo da li su njegovi predmeti display = ""
                if(predmeti[i].style.display === ""){
                    brojac = brojac + 1;                                        //kako vidimo da jesu test prolazi
                }
            }
            assert.equal(brojac, 3,"Broj predmeta treba biti 3");
          });
    });
    describe('pretragaGodina()', function() {
        it('Provjera da su sakriveni svi predmeti koji nisu na drugoj godini kada se pozove pretragaGodina(2)', function() {
            let predmeti = document.getElementsByClassName("predmeti");
            Pretraga.postaviElemente(predmeti);
            let brojac = 0;                                                     //treba da sakrijemo predmete prve godine
            Pretraga.pretragaGodina(2);                                         //test smo uradili na nacin da ukoliko su prikazani svi predmeti druge godine ukupno 9 
            for(let i=0; i<predmeti.length; i++){                               //brojac treba biti 9 
                if(predmeti[i].style.display === ""){
                    brojac = brojac + 1;
                }
            }
            assert.equal(brojac, 9,"Broj predmeta treba biti 9");
        });
        it('Provjera da se prikazuju svi predmeti kada se unese neka vrijednost različita od 1 i 2 za metodu pretragaGodina', function() {
            let predmeti = document.getElementsByClassName("predmeti");
            Pretraga.postaviElemente(predmeti);
            let brojac = 0;                                                    //ukoliko unesemo broj razlicit od 1 i 2 prikazuju se svi predmeti tako da naš brojac treba biti
            Pretraga.pretragaGodina(1.1);
            for(let i=0; i<predmeti.length; i++){                              //19 sto je tačno u našem slučaju
                if(predmeti[i].style.display === ""){
                    brojac = brojac + 1;
                }
            }
            assert.equal(brojac, 19,"Broj predmeta treba biti 19");
        });
    });    
});